// console.log("Hello world!");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:


	let myName = "Full Name: Christ James";
	console.log(myName);
	
	let lastName = "Last Name: Ocañada"
	console.log(lastName);
	
	let myAge = "Age:";
	const old = 26;
	console.log(myAge, old);
	
	let hobbies = "Hobbies:";
	console.log(hobbies);

	let myHobbies = ["playing Chess", "playing Dota", "watching Anime"];
	console.log(myHobbies);

	let workAddress = "Work Address:"
	console.log(workAddress);

	let address = {
		 
			houseNumber: "857",
			street: "bayabas",
			barangay: "Basak San Nicolas",
			city: "Cebu City",
			country: "Philippines"
			
	};

	console.log(address);
	
	let fullName = "Full Name: Steve Rogers";
	console.log(fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		userName: "Captain America",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	};
	console.log("My Full Profile: ")
	console.log(profile);

	let nameFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + nameFriend);

	const lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);








	/*
		1. Create variables to store to the following user details:

		-first name - String
		-last name - String
		-age - Number
		-hobbies - Array
		-work address - Object

			-The hobbies array should contain at least 3 hobbies as Strings.
			-The work address object should contain the following key-value pairs:

				houseNumber: <value>
				street: <value>
				city: <value>
				state: <value>

		Log the values of each variable to follow/mimic the output.

		Note:
			-Name your own variables but follow the conventions and best practice in naming variables.
			-You may add your own values but keep the variable names and values Safe For Work.
	*/

		//Add your variables and console log for objective 1 here:




	/*			
		2. Debugging Practice - Identify and implement the best practices of creating and using variables 
		   by avoiding errors and debugging the following codes:

				-Log the values of each variable to follow/mimic the output.
	*/	